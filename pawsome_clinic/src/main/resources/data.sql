INSERT INTO `roles` (`name`) VALUES
    ('USER'),
    ('ADMIN');

INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `phone`, `enabled`, `file_name`) VALUES
    ('user', 'user', 'user', '$2y$12$l5HcryDWeN43ncgugtrNYuvXDCsBZxVo1FezzT9m9ljlm9L9N19hK', 'user@user.com', 26467456, true, 'userdefault.jpg'),
    ('admin1', 'admin2', 'admin3', '$2y$12$wcwhDBLOz02cFTlmPN4sxO3aht2LHZ5Oepbd0VXM6YhKI1tpiPKtC', 'admin@admin.com', 23534536, true, 'userdefault.jpg'),
    ('test', 'test', 'test', '$2y$12$h8rO9FNHvyirKLx1qx0lOudpzDltQGVtJj5IqHmP0EAKx49uyuFfu', 'test@test.com', 23778536, true, 'userdefault.jpg'),
    ('test2', 'test2', 'test2', '$2y$12$kDuekdBEaCVj0m8zM8KIHe9G02DXSZ4IW6YCmv7Xu1.BZh3i7EETy', 'test2@test.com', 23778536, true, 'userdefault.jpg');

INSERT INTO `users_roles` (`user_id`, `role_id`) VALUES
    (1, 1),
    (2, 2),
    (3, 1),
    (4, 1);

INSERT INTO `pet_types` (`name`) VALUES
    ('Cat'),
    ('Dog'),
    ('Rabbit'),
    ('Bird'),
    ('Hamster'),
    ('Reptile');

INSERT INTO `breeds` (`name`, `pet_type_pet_type_id`) VALUES
    ('Golden Retriever', 2),
    ('Pomeranian', 2),
    ('French Bulldog', 2),
    ('Dachshund', 2),
    ('Poodle', 2),
    ('Sphynx', 1),
    ('Siamese', 1),
    ('Persian', 1),
    ('British Shorthair', 1),
    ('Toyger', 1),
    ('Lovebird', 4),
    ('Dove', 4),
    ('Parrotlet', 4),
    ('Canary', 4),
    ('Cockatiel', 4),
    ('Other', 1),
    ('Other', 2),
    ('Other', 3),
    ('Other', 4),
    ('Other', 5),
    ('Other', 6);

INSERT INTO `pets` (`name`,
                    `birth_date`,
                    `age`,
                    `chip_no`,
                    `gender`,
                    `breed_breed_id`,
                    `pet_type_pet_type_id`,
                    `user_user_id`,
                    `file_name`
                    )
            VALUES
                    ('Fluffy',
                    '2016-08-21',
                    4,
                    'SJDHF3JAD098378',
                    'male',
                    2,
                    2,
                    2,
                    'default.jpg'
                    ),
                    ('Snowball',
                    '2014-05-16',
                    6,
                    'SOFIDNVJD076347',
                    'female',
                    9,
                    1,
                    1,
                    'default.jpg'
                    );

INSERT INTO `procedures` (`duration_in_min`, `price`, `name`) VALUES
                         (15, 20, 'Consultation'),
                         (20, 35, 'Dermatologist consultation'),
                         (30, 35, 'Behavioural Consultation'),
                         (20, 20, 'Surgeon Consultation'),
                         (20, 40, 'Home Visitation'),
                         (15, 10, 'Pet Passport'),
                         (15, 17, 'Microchip'),
                         (20, 40, 'Home Visitation'),
                         (10, 10, 'Rabies Vaccination'),
                         (10, 20, 'Combined Vaccination'),
                         (15, 10, 'Nail Trimming'),
                         (15, 10, 'Ear Cleaning'),
                         (60, 40, 'Teeth cleaning'),
                         (20, 7, 'Tooth removal'),
                         (120, 45, 'Neutering - Male'),
                         (120, 55, 'Neutering - Female'),
                         (120, 60, 'General Surgery'),
                         (30, 20, 'X-ray'),
                         (30, 20, 'Ultrasonography'),
                         (20, 40, 'Blood Test');


