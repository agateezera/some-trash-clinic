package com.pawsomepetclinic.dto;
import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PetDto {

    Long id;
    Long userId;
    private String name;
    private String birthDate;
    private long age;
    private String gender;
    public String chipNo;
    private String fileName;
    private Long petTypeId;
    private Long breedId;
}
