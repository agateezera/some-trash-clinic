package com.pawsomepetclinic.controller;

import com.pawsomepetclinic.model.Procedure;
import com.pawsomepetclinic.model.pets.Pet;
import com.pawsomepetclinic.service.ProcedureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
@RestController
public class ProcedureController {

    ProcedureService procedureService;

    @Autowired
    public ProcedureController(ProcedureService procedureService) {
        this.procedureService = procedureService;
    }

    @GetMapping("/procedures")
    public List<Procedure> getAllProcedures() {
        return procedureService.getAllProcedures();
    }
}
