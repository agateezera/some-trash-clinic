package com.pawsomepetclinic.controller;

import com.pawsomepetclinic.dto.PetDto;
import com.pawsomepetclinic.exception.ResourceNotFoundException;
import com.pawsomepetclinic.model.pets.Breed;
import com.pawsomepetclinic.model.pets.Pet;
import com.pawsomepetclinic.model.pets.PetType;
import com.pawsomepetclinic.service.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping({ "/pets" })
public class PetController {

    private PetService petService;

    @Autowired
    public PetController(PetService petService) {
        this.petService = petService;
    }

    @GetMapping("")
    public List<Pet> getAllPets() {
        return petService.getAllPets();
    }

    @GetMapping("/types")
    public List<PetType> getAllPetTypes() {
        return petService.getAllPetTypes();
    }

    @GetMapping("/breeds")
    public List<Breed> getAllBreeds() {
        return petService.getAllBreeds();
    }


    @GetMapping("/{id}")
    public ResponseEntity<Pet> getPetById(@PathVariable(value = "id") Long petId)
            throws ResourceNotFoundException {
        return petService.getPetById(petId);
    }

    @PostMapping
    @Transactional
    public ResponseEntity<Void> createPet(@RequestBody PetDto petDto) throws ResourceNotFoundException {
        petService.save(petDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

//    @PostMapping("")
//    public Pet createPet(@RequestBody Pet pet, MultipartFile file) throws IOException {
//        return petService.createPet(pet, file);
//    }

    @PutMapping("/{id}")
    public ResponseEntity<Pet> updatePet(@PathVariable(value = "id") Long petId,
                                           @RequestBody Pet petDetails) throws ResourceNotFoundException {
        return petService.updatePet(petId, petDetails);
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deletePet(@PathVariable(value = "id") Long petId)
            throws ResourceNotFoundException {
        return petService.deletePet(petId);
    }


}
