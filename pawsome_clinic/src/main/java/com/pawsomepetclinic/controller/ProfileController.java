package com.pawsomepetclinic.controller;

import com.pawsomepetclinic.service.PetService;
import com.pawsomepetclinic.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

public class ProfileController {

    private UserService userService;
    private PetService petService;

    @Autowired
    public ProfileController(UserService userService, PetService petService) {
        this.userService = userService;
        this.petService = petService;
    }

    @GetMapping("/profile")
    public void showProfilePage() {

    }
}
