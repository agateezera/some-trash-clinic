package com.pawsomepetclinic.service;

import com.pawsomepetclinic.dto.AppointmentDto;
import com.pawsomepetclinic.exception.ResourceNotFoundException;
import com.pawsomepetclinic.model.Appointment;
import com.pawsomepetclinic.model.Procedure;
import com.pawsomepetclinic.model.persons.User;
import com.pawsomepetclinic.repository.AppointmentRepository;
import com.pawsomepetclinic.repository.ProcedureRepository;
import com.pawsomepetclinic.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class AppointmentService {

    private final AppointmentRepository appointmentRepository;
    private final UserRepository userRepository;
    private final ProcedureRepository procedureRepository;

    //    String str = "1986-04-08 12:30";
//    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
//    LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

    public List<Appointment> getAllAppointments() {
        return appointmentRepository.findAll();
    }

    public ResponseEntity<Appointment> getAppointmentById(Long appointmentId)
            throws ResourceNotFoundException {
        Appointment appointment = appointmentRepository.findById(appointmentId)
                .orElseThrow(() -> new ResourceNotFoundException("Pet not found for this id :: " + appointmentId));
        return ResponseEntity.ok().body(appointment);
    }

    public void save(AppointmentDto appointmentDto) throws ResourceNotFoundException {

        User user = userRepository.findById(appointmentDto.getUserId()).
                orElseThrow(() -> new ResourceNotFoundException("User not found"));
        Procedure procedure = procedureRepository.findById(appointmentDto.getProcedureId()).
                orElseThrow(() -> new ResourceNotFoundException("Procedure not found"));
        LocalDate appointmentDate = LocalDate.parse(appointmentDto.getAppointmentDate());
        System.out.println(appointmentDate);

        Appointment appointment = new Appointment();

        appointment.setUser(user);
        appointment.setAppointmentDate(appointmentDate);
        appointment.setAppointmentTime(appointmentDto.getAppointmentTime());
        appointment.setProcedure(procedure);
        appointment.setPetName(appointmentDto.getPetName());

        appointmentRepository.save(appointment);
    }

    public ResponseEntity<Appointment> updateAppointment(Long appointmentId, Appointment appointmentDetails) throws ResourceNotFoundException {
        Appointment appointment = appointmentRepository.findById(appointmentId)
                .orElseThrow(() -> new ResourceNotFoundException("appointment not found for this id :: " + appointmentId));

        appointment.setAppointmentDate(appointmentDetails.getAppointmentDate());
        appointment.setAppointmentTime(appointmentDetails.getAppointmentTime());
        appointment.setProcedure(appointmentDetails.getProcedure());
        appointment.setPetName(appointmentDetails.getPetName());


        final Appointment updatedAppointment = appointmentRepository.save(appointment);
        return ResponseEntity.ok(updatedAppointment);
    }

    public Map<String, Boolean> deleteAppointment(Long appointmentId)
            throws ResourceNotFoundException {
        Appointment appointment = appointmentRepository.findById(appointmentId)
                .orElseThrow(() -> new ResourceNotFoundException("Appointment not found for this id :: " + appointmentId));

        appointmentRepository.delete(appointment);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
