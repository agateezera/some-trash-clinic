package com.pawsomepetclinic.service;

import com.pawsomepetclinic.exception.ResourceNotFoundException;
import com.pawsomepetclinic.model.persons.Authorities;
import com.pawsomepetclinic.model.persons.User;
import com.pawsomepetclinic.model.pets.Pet;
import com.pawsomepetclinic.repository.PetRepository;
import com.pawsomepetclinic.repository.RoleRepository;
import com.pawsomepetclinic.repository.UserRepository;
import com.pawsomepetclinic.util.FileNameGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PetRepository petRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private static final Path fileUploadDirectory = Paths.get("./src/main/uploads/images/user");

    @Autowired
    public UserService(UserRepository userRepository,
                       RoleRepository roleRepository,
                       PetRepository petRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.petRepository = petRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public ResponseEntity<User> getUserById(Long userId)
            throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));
        return ResponseEntity.ok().body(user);
    }

    public User createUser(User user) {

        user.setEnabled(true);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setFileName("default.png");

        if (user.getRoles().isEmpty()) {
            user.getRoles().add(roleRepository.getByName(Authorities.USER.name()));
        }

        return userRepository.save(user);
    }

    public User saveProfilePhoto(User user, MultipartFile file) throws IOException {
        if (file == null || file.isEmpty()) {
            user.setFileName("default.png");
            userRepository.save(user);
            return user;
        } else {
            String toDelete = user.getFileName();
            String fileName = FileNameGenerator.getRandomFilename(file);
            Path filePath = Paths.get(fileUploadDirectory + "/" + fileName);
            try {
                Files.write(filePath, file.getBytes());
                user.setFileName(fileName);
                userRepository.save(user);
                if (!("default.png".equals(toDelete))) {
                    Path pathToDelete = Paths.get(fileUploadDirectory + "/" + toDelete);
                    Files.delete(pathToDelete);
                }
                return user;
            } catch (Exception e) {
            }
        }

        return user;
    }

    public User deleteProfilePhoto(User user) throws IOException {
        if (!("default.png").equals(user.getFileName())) {
            Path deletePath = Paths.get(fileUploadDirectory + "/" + user.getFileName());
            Files.delete(deletePath);
            user.setFileName("default.png");
        }

        return user;
    }

    public User updateUserPassword(User user, String password) {
        user.setPassword(bCryptPasswordEncoder.encode(password));
        userRepository.save(user);
        return user;
    }

    public ResponseEntity<User> updateUser(Long userId, User userDetails) throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));

        user.setFirstName(userDetails.getFirstName());
        user.setLastName(userDetails.getLastName());
        user.setEmail(userDetails.getEmail());
        user.setPhone(userDetails.getPhone());

        final User updatedUser = userRepository.save(user);
        return ResponseEntity.ok(updatedUser);
    }

    public Map<String, Boolean> deleteUser(Long userId)
            throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));

        userRepository.delete(user);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    public String getCurrentUser(){

        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();
    }

    public boolean checkForAdmin(){

        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getAuthorities()
                .stream().anyMatch(r->r.equals(Authorities.ADMIN.name()));
    }

    public User getCurrentUserDetails(String username){

        return userRepository.findByUsername(username);

    }


}
