package com.pawsomepetclinic.service;

import com.pawsomepetclinic.dto.PetDto;
import com.pawsomepetclinic.exception.ResourceNotFoundException;
import com.pawsomepetclinic.model.persons.User;
import com.pawsomepetclinic.model.pets.Breed;
import com.pawsomepetclinic.model.pets.Pet;
import com.pawsomepetclinic.model.pets.PetType;
import com.pawsomepetclinic.repository.BreedRepository;
import com.pawsomepetclinic.repository.PetRepository;
import com.pawsomepetclinic.repository.PetTypeRepository;
import com.pawsomepetclinic.repository.UserRepository;
import com.pawsomepetclinic.util.FileNameGenerator;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class PetService {

    private final UserRepository userRepository;
    private final PetRepository petRepository;
    private final PetTypeRepository petTypeRepository;
    private final BreedRepository breedRepository;

    private static final Path fileUploadDirectory = Paths.get("./src/main/uploads/images/pet");

//    @Autowired
//    public PetService(UserRepository userRepository, PetRepository petRepository, PetMapper petMapper) {
//        this.userRepository = userRepository;
//        this.petRepository = petRepository;
//        this.petMapper = petMapper;
//    }

    public List<Pet> getAllPets() {
        return petRepository.findAll();
    }
    public List<PetType> getAllPetTypes() { return petTypeRepository.findAll(); }
    public List<Breed> getAllBreeds() { return breedRepository.findAll(); }


    public ResponseEntity<Pet> getPetById(Long petId)
            throws ResourceNotFoundException {
        Pet pet = petRepository.findById(petId)
                .orElseThrow(() -> new ResourceNotFoundException("Pet not found for this id :: " + petId));
        return ResponseEntity.ok().body(pet);
    }

    //MultipartFile file
    public void save(PetDto petDto) throws ResourceNotFoundException {

        String defaultFileName = "default.png";

        User user = userRepository.findById(petDto.getUserId()).
                orElseThrow(() -> new ResourceNotFoundException("User not found"));
        PetType petType = petTypeRepository.findById(petDto.getPetTypeId()).
                orElseThrow(() -> new ResourceNotFoundException("Pet type not found"));
        Breed breed = breedRepository.findById(petDto.getBreedId()).
                orElseThrow(() -> new ResourceNotFoundException("Breed not found"));
        LocalDate birthDate = LocalDate.parse(petDto.getBirthDate());
        System.out.println(petDto.getBirthDate());

        Pet pet = new Pet();

        pet.setUser(user);
        pet.setName(petDto.getName());
        pet.setChipNo(petDto.getChipNo());
        pet.setBirthDate(birthDate);
        pet.setAge(birthDate);
        pet.setGender(petDto.getGender());
        pet.setFileName(defaultFileName); //HAVE TO IMPLEMENT IMAGE UPLOAD
        pet.setPetType(petType);
        pet.setBreed(breed);

        //        Pet pet = petMapper.map(petDto, user);
        petRepository.save(pet);


//        if (file == null || file.isEmpty()) {
//            pet.setFileName("default.png");
////            pet.setUser(user);
//            petRepository.save(pet);
//            return pet;
//        } else {
//            String fileName = FileNameGenerator.getRandomFilename(file);
//            Path filePath = Paths.get(fileUploadDirectory + "/" + fileName);
//            try {
//                Files.write(filePath, file.getBytes());
//                pet.setFileName(fileName);
////                pet.setUser(user);
//                petRepository.save(pet);
//                return pet;
//            } catch (Exception e) {
//
//            }
//        }
//        return pet;
    }

    public ResponseEntity<Pet> updatePet(Long petId, Pet petDetails) throws ResourceNotFoundException {
        Pet pet = petRepository.findById(petId)
                .orElseThrow(() -> new ResourceNotFoundException("Pet not found for this id :: " + petId));

        pet.setFileName(petRepository.findById(pet.getId()).orElseThrow(EntityNotFoundException::new).getFileName());
        pet.setName(petDetails.getName());
        pet.setChipNo(petDetails.getChipNo());
        pet.setUser(petDetails.getUser());

        final Pet updatedPet = petRepository.save(pet);
        return ResponseEntity.ok(updatedPet);

    }

    public Pet updatePetPicture(Pet pet, MultipartFile file) {
        if (file.isEmpty()) {
            return pet;
        } else {
            String toDelete = pet.getFileName();
            String fileName = FileNameGenerator.getRandomFilename(file);
            Path filePath = Paths.get(fileUploadDirectory + "/" + fileName);
            try {
                Files.write(filePath, file.getBytes());
                pet.setFileName(fileName);
                if (!("default.png".equals(toDelete))) {
                    Path pathToDelete = Paths.get(fileUploadDirectory + "/" + toDelete);
                    Files.delete(pathToDelete);
                }
            } catch (Exception e) {
            }

            return pet;
        }
    }

    public Pet deletePetPicture(Pet pet) throws IOException {
        if (!("default.png").equals(pet.getFileName())) {
            Path deletePath = Paths.get(fileUploadDirectory + "/" + pet.getFileName());
            Files.delete(deletePath);
            pet.setFileName("default.png");
        }

        return pet;
    }

    public Map<String, Boolean> deletePet(Long petId)
            throws ResourceNotFoundException {
        Pet pet = petRepository.findById(petId)
                .orElseThrow(() -> new ResourceNotFoundException("Pet not found for this id :: " + petId));

        petRepository.delete(pet);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

}
