package com.pawsomepetclinic.service;

import com.pawsomepetclinic.model.Procedure;
import com.pawsomepetclinic.model.pets.Pet;
import com.pawsomepetclinic.repository.ProcedureRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProcedureService {

    private final ProcedureRepository procedureRepository;

    public List<Procedure> getAllProcedures() {
        return procedureRepository.findAll();
    }
}
