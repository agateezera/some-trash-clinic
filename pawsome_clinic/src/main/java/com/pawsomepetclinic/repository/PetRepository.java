package com.pawsomepetclinic.repository;

import com.pawsomepetclinic.model.pets.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet, Long> {
    Pet findByChipNo(String chipNo);
}
