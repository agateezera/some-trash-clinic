package com.pawsomepetclinic.repository;

import com.pawsomepetclinic.model.pets.PetType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetTypeRepository extends JpaRepository<PetType, Long> {
}
