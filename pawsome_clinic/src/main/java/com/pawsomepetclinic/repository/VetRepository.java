package com.pawsomepetclinic.repository;

import com.pawsomepetclinic.model.persons.Vet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VetRepository extends JpaRepository<Vet, Long> {
}
