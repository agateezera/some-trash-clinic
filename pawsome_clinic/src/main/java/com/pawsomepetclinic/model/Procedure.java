package com.pawsomepetclinic.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "procedures")
public class Procedure {

    @Id
    @Column(name = "procedure_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer durationInMin;
    private Double price;
    private String name;

//    @OneToMany(mappedBy = "procedure",
//               cascade = CascadeType.ALL,
//               orphanRemoval = true)
//    @JsonIgnoreProperties({"procedure"})
//    private Set<Appointment> appointments = new HashSet<>();

    public Procedure(Integer durationInMin, Double price, String name) {
        this.durationInMin = durationInMin;
        this.price = price;
        this.name = name;
    }
}
