package com.pawsomepetclinic.model.persons;

import com.pawsomepetclinic.model.Appointment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "vets")
public class Vet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @OneToMany( mappedBy = "vet",
//            cascade = CascadeType.ALL,
//            orphanRemoval = true)
//    private Set<Appointment> appointments = new HashSet<>();
}
