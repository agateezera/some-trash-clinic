package com.pawsomepetclinic.model.persons;

public enum Authorities {
    ADMIN,
    USER
}
