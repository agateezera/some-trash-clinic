package com.pawsomepetclinic.model.persons;

import com.fasterxml.jackson.annotation.*;
import com.pawsomepetclinic.model.Appointment;
import com.pawsomepetclinic.model.pets.Pet;
import lombok.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.PERSIST;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "First name field cannot be empty")
    private String firstName;
    @NotNull(message = "Last name field cannot be empty")
    private String lastName;
    private String username;
    @NotNull(message = "Password field cannot be empty")
    @Size(min = 4)
    private String password;
    @NotNull(message = "Email field cannot be empty")
    @Email(message = "Email should be valid")
    private String email;
    @NotNull(message = "Phone field cannot be empty")
    @Size(min = 8, max = 8)
    private String phone;
    private String fileName;
    private boolean enabled;

    @ManyToMany(cascade={PERSIST, DETACH}, fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles = new HashSet<>();

    @OneToMany( mappedBy = "user",
                cascade = CascadeType.ALL,
                fetch = FetchType.LAZY,
                orphanRemoval = true)
    @JsonIgnoreProperties({"user"})
    private Set<Pet> pets = new HashSet<>();

    @OneToMany( mappedBy = "user",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            orphanRemoval = true)
    @JsonIgnoreProperties({"user"})
    private Set<Appointment> appointments = new HashSet<>();


    public User(String firstName, String lastName, String username, String password, String email, String phone, boolean enabled) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.enabled = enabled;
    }

    public void addPet(Pet pet) {
        pets.add(pet);
    }

    public void removePet(Pet pet) {
        pets.remove(pet);
    }

//    public void addAppointment(Appointment appointment) {
//        appointments.add(appointment);
//    }
//
//    public void removeAppointment(Appointment appointment) {
//        appointments.remove(appointment);
//    }

//    @JsonIgnore
//    public void setAppointments(Set<Appointment> appointments) {
//        this.appointments = appointments;
//    }
}

