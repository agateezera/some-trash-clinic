package com.pawsomepetclinic.model;

import com.fasterxml.jackson.annotation.*;
import com.pawsomepetclinic.model.persons.User;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "appointments")
public class Appointment {

    @Id
    @Column(name = "appointment_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate appointmentDate;
    private String appointmentTime;
    private String petName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "procedure_id")
    @JsonIgnoreProperties({"appointments"})
    private Procedure procedure;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @JsonIgnoreProperties({"appointments"})
    private User user;

    @JsonIgnore
    public User getUser() {
        return user;
    }


    public Appointment(LocalDate appointmentDate, String appointmentTime, String petName, Procedure procedure, User user) {
        this.appointmentDate = appointmentDate;
        this.appointmentTime = appointmentTime;
        this.petName = petName;
        this.procedure = procedure;
        this.user = user;
    }


}
