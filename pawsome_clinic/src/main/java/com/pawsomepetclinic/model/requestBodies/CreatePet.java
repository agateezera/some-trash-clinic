package com.pawsomepetclinic.model.requestBodies;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pawsomepetclinic.model.persons.User;
import com.pawsomepetclinic.model.pets.Pet;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreatePet {

    User user;
    Pet pet;

    public CreatePet(User user, Pet pet) {
        this.user = user;
        this.pet = pet;
    }
}
