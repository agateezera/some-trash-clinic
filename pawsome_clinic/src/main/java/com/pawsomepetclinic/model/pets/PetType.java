package com.pawsomepetclinic.model.pets;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.pawsomepetclinic.model.pets.Breed;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "pet_types")
public class PetType {

    @Id
    @Column(name = "pet_type_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany( mappedBy = "PetType",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
//    @JsonManagedReference
    @JsonIgnoreProperties({"PetType"})
    private Set<Breed> breeds = new HashSet<>();


    public PetType(String name) {
        this.name = name;
    }

    public void addBreed(Breed breed) {
        breeds.add(breed);
    }

    public void removeBreed(Breed breed) {
        breeds.remove(breed);
    }

    @JsonIgnore
    public Set<Breed> getBreeds() {
        return breeds;
    }
}
