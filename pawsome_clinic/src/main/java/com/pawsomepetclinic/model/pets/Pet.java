package com.pawsomepetclinic.model.pets;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pawsomepetclinic.model.MedicalRecord;
import com.pawsomepetclinic.model.persons.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "pets")
public class Pet {

    @Id
    @Column(name = "pet_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Name field cannot be empty")
    private String name;
    @NotNull(message = "Birth data cannot be empty")
    @PastOrPresent
    private LocalDate birthDate;
    private long age;
    private String gender;
    @Size(min = 15, max = 16)
    private String chipNo;
    private String fileName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"pets"})
//    @JsonBackReference
    private User user;

    public Pet(@NotNull(message = "Name field cannot be empty") String name, @PastOrPresent @Size(min = 15, max = 16) String chipNo, String fileName) {
        this.name = name;
        this.chipNo = chipNo;
        this.fileName = fileName;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private PetType PetType;

    @ManyToOne(fetch = FetchType.LAZY)
    private Breed breed;

//    @OneToMany( mappedBy = "pet",
//            cascade = CascadeType.ALL,
//            orphanRemoval = true)
//    private Set<MedicalRecord> medicalRecords = new HashSet<>();

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public void setAge(LocalDate birthDate) {
        LocalDate now = LocalDate.now();
        //should calculate in months for baby animals
//        long petAge = Period.between(birthDate, now).getYears();
//        if (petAge < 1) {
//            petAge = Period.between(birthDate, now).getMonths();
//        }
        this.age = Period.between(birthDate, now).getYears();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet )) return false;
        return id != null && id.equals(((Pet) o).getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @JsonBackReference
    public User getUser() {
        return user;
    }
}
