package com.pawsomepetclinic.model;

import com.pawsomepetclinic.model.persons.User;
import com.pawsomepetclinic.model.pets.Pet;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "medical_records")
public class MedicalRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    private Procedure procedure;

    @ManyToOne(fetch = FetchType.LAZY)
    private Pet pet;

    public MedicalRecord(Date date, Procedure procedure, Pet pet) {
        this.date = date;
        this.procedure = procedure;
        this.pet = pet;
    }
}
