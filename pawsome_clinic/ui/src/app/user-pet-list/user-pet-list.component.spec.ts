import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPetListComponent } from './user-pet-list.component';

describe('UserPetListComponent', () => {
  let component: UserPetListComponent;
  let fixture: ComponentFixture<UserPetListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserPetListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
