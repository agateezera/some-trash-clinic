import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ProcedureService {

  private baseUrl = 'http://localhost:8080/procedures';

  constructor(private http: HttpClient) { }

  getProcedureList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

}
