import {PetType} from "./petType";
import {Breed} from "./breed";
import {User} from "./user";


export class Pet {
  id?: number;
  name: string;
  birthDate: string;
  age?: number;
  gender: string;
  chipNo: string;
  fileName?: string;
  userId: number;
  type: string;
  petTypeId: number;
  breedId: number;
  user?: User;
  petType?: PetType;
  breed?: Breed;
  // medicalRecords?: [];

}
