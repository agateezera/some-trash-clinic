import {Procedure} from "./procedure";

export class Appointment {
  id: number;
  appointmentDate: string;
  appointmentTime: string;
  userId: number;
  petName?: string;
  // petId?: number;
  procedureId: number;
  procedure: Procedure;
}
