export class Breed {
  id: number;
  name: string;
  petTypeId?: number;
}
