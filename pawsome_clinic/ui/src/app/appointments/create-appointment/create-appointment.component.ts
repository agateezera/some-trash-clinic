import { Component, OnInit } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {BsDatepickerConfig} from "ngx-bootstrap/datepicker";
import {PetService} from "../../pets/pet.service";
import {Router} from "@angular/router";
import {DatePipe} from "@angular/common";
import {User} from "../../models/user";
import { UserService } from 'src/app/users/user.service';
import {ProcedureService} from "../../procedures/procedure.service";
import { ActivatedRoute } from '@angular/router';
import {Procedure} from "../../models/procedure";
import {Appointment} from "../../models/appointment";
import {AppointmentService} from "../appointment.service";
import {VisitTimes} from "../../models/visitTimes";
import {Pet} from "../../models/pet";

@Component({
  selector: 'app-create-appointment',
  templateUrl: './create-appointment.component.html',
  styleUrls: ['./create-appointment.component.css']
})
export class CreateAppointmentComponent implements OnInit {

  public destroyed = new Subject<any>();

  datePickerConfig: Partial<BsDatepickerConfig>;

  appointment: Appointment = new Appointment();
  user: User;
  pets: Array<Pet>;
  procedures: Observable<Procedure[]>;
  appointmentDate: Date;
  id: number;
  stringDate: string;
  submitted = false;
  now: Date;
  day: number;
  month: number;
  year: number;


  appointmentTimes: VisitTimes[] = [
    {id: 1, time: "8:00"}, {id: 2, time: "8:30"}, {id: 3, time: "9:00"}, {id: 4, time: "9:30"},
    {id: 5, time: "10:00"}, {id: 6, time: "10:30"}, {id: 7, time: "11:00"}, {id: 8, time: "11:30"},
    {id: 9, time: "12:00"}, {id: 10, time: "12:30"}, {id: 11, time: "13:00"}, {id: 12, time: "13:30"},
    {id: 13, time: "14:00"}, {id: 14, time: "14:30"}, {id: 15, time: "15:00"}, {id: 16, time: "15:30"},
    {id: 17, time: "16:00"}, {id: 18, time: "16:30"}, {id: 19, time: "17:00"}, {id: 20, time: "17:30"},
    {id: 21, time: "18:00"}, {id: 22, time: "18:30"}, {id: 23, time: "19:00"}, {id: 24, time: "19:30"},
    {id: 25, time: "20:00"},
    ];


  constructor(private petService: PetService,
              private userService: UserService,
              private procedureService: ProcedureService,
              private appointmentService: AppointmentService,
              private router: Router,
              private datePipe: DatePipe,
              private route: ActivatedRoute) {
    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        showWeekNumbers: false,
        minDate: new Date(),
        maxDate: new Date(this.year, this.month + 1, this.day),
        dateInputFormat: 'YYYY-MM-DD'
      });
  }

  ngOnInit() {
    this.user = new User();
    this.reloadData();
    this.now = new Date();
    this.day = this.now.getDay();
    this.month = this.now.getMonth();
    this.year = this.now.getFullYear();

    this.userService.getUser(this.id)
      .subscribe(data => {
        console.log(data)
        this.user = data;
        this.pets = this.user.pets;
      }, error => console.log(error));
  }

  reloadData() {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.procedures = this.procedureService.getProcedureList();
  }

  newAppointment(): void {
    this.submitted = false;
    this.appointment = new Appointment();
  }

  save() {
    this.dateToString();
    console.log(this.stringDate);
    this.appointment.appointmentDate = this.stringDate;
    console.log(this.appointment.appointmentDate);
    this.appointment.userId = this.id;
    this.appointmentService
      .createAppointment(this.appointment).subscribe(data => {
        this.appointment = new Appointment();
        this.gotoList();
      },
      error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/users']);
  }

  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();
  }

  dateToString(){
    this.stringDate =this.datePipe.transform(this.appointmentDate, 'yyyy-MM-dd');
  }

}
