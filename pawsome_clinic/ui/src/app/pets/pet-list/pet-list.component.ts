import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {Pet} from "../../models/pet";
import {PetService} from "../pet.service";
import {User} from "../../models/user";
import { UserService } from 'src/app/users/user.service';

@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html',
  styleUrls: ['./pet-list.component.css']
})
export class PetListComponent implements OnInit {

  pets: Observable<Pet[]>;
  user: User;
  myPetData: any;
  petObjects: Pet[];

  constructor(private petService: PetService,
              private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.pets = this.petService.getPetList();
    this.petService.getPetList()
      .subscribe(pets => {
        this.myPetData.rowData = this.petObjects as Pet[];
      })
  }

  deletePet(id: number) {
    this.petService.deletePet(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  petDetails(id: number){
    this.router.navigate(['pet-details', id]);
  }

  updatePet(id: number){
    this.router.navigate(['update-pet', id]);
  }

}
