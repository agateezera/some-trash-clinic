import { Component, OnInit } from '@angular/core';
import {NavigationEnd, Router, RouterEvent} from "@angular/router";
import {Pet} from "../../models/pet";
import {PetService} from "../pet.service";
import {BsDatepickerConfig} from "ngx-bootstrap/datepicker";
import {PetType} from "../../models/petType";
import {filter} from "rxjs/operators";
import {Observable, Subject} from "rxjs";
import {Breed} from "../../models/breed";
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-create-pet',
  templateUrl: './create-pet.component.html',
  styleUrls: ['./create-pet.component.css']
})
export class CreatePetComponent implements OnInit {

  public destroyed = new Subject<any>();
  public types: Observable<PetType[]>;
  public breeds: Observable<Breed[]>;

  datePickerConfig: Partial<BsDatepickerConfig>;

  pet: Pet = new Pet();
  petBirthDate: Date;
  stringDate: string;
  submitted = false;

  constructor(private petService: PetService,
              private router: Router,
              public datePipe: DatePipe) {
    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        showWeekNumbers: false,
        minDate: new Date(1990, 0, 1),
        maxDate: new Date(),
        dateInputFormat: 'YYYY-MM-DD'
      });
  }

  ngOnInit() {
    this.reloadData();

  }

  reloadData() {
    this.types = this.petService.getPetTypeList();
    this.breeds = this.petService.getBreeds();
  }

  newPet(): void {
    this.submitted = false;
    this.pet = new Pet();
  }

  save() {
    this.dateToString();
    this.pet.birthDate = this.stringDate;
    this.petService
      .createPet(this.pet).subscribe(data => {
        console.log(data)
        this.pet = new Pet();
        this.gotoList();
      },
      error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/pets']);
  }

  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();
  }

  dateToString(){
    this.stringDate =this.datePipe.transform(this.petBirthDate, 'yyyy-MM-dd');
  }

}
