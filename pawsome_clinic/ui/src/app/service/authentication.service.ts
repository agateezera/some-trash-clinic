import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from "rxjs/operators";
import {BehaviorSubject, Observable} from "rxjs";
import {User} from "../models/user";

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

  USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'

  private url: string;
  public username: String;
  public password: String;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;


  //--------------
  private loginStatus = new BehaviorSubject<boolean>(this.isUserLoggedIn());
  private UserName = new BehaviorSubject<string>(sessionStorage.getItem('username'));

  //--------------

  constructor(private httpClient: HttpClient) {
    this.url = 'http://localhost:8080/users/current';
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: String, password: String) {
    const headers = new HttpHeaders({
      Authorization: 'Basic ' + btoa(username + ':' + password),
    });

    let promise = new Promise((resolve, reject) => {
      this.httpClient
        .get<string>(this.url, { headers })
        .subscribe((currentUser) => {
          console.log(currentUser);
          sessionStorage.setItem('username', currentUser);
          let authString = 'Basic ' + btoa(username + ':' + password);
          sessionStorage.setItem('basicauth', authString);
          this.username = username;
          this.password = password;
          this.loginStatus.next(true);
          this.registerSuccessfulLogin(username,password);
          resolve();
        });
    });
    return promise;
  }


  createBasicAuthToken(username: String, password: String) {
    return 'Basic ' + window.btoa(username + ":" + password)
  }

  registerSuccessfulLogin(username, password) {
    sessionStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME, username)
  }

  logout() {
    sessionStorage.removeItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
    this.username = null;
    this.password = null;
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
    if (user === null) return false;
    return true
  }

  getLoggedInUserName() {
    let user = sessionStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
    if (user === null) return '';
    return user
  }

  //------------------
  get isLoggedIn()
  {
    return this.loginStatus.asObservable();
  }

  get currentUserName()
  {
    return this.UserName.asObservable();
  }

}
