import { Component, OnInit } from '@angular/core';
import { User } from "../../models/user";
import { UserService } from '../user.service';
import {Observable, Subject} from "rxjs";
import {NavigationEnd, Router, RouterEvent} from "@angular/router";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-user',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  public destroyed = new Subject<any>();
  users: Observable<User[]>;

  constructor(private userService: UserService,
              private router: Router) {}

  ngOnInit() {
    this.reloadData();
    this.router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.reloadData();
    });
  }

  reloadData() {
    this.users = this.userService.getUserList();
  }

  deleteUser(id: number) {
    this.userService.deleteUser(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  userDetails(id: number){
    this.router.navigate(['details', id]);
  }

  updateUser(id: number){
    this.router.navigate(['update', id]);
  }

  createPet(id: number) {
    this.router.navigate(['user', id, 'add-pet']);
  }

  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();
  }

  createAppointment(id: number) {
    this.router.navigate(['user', id, 'add-appointment']);
  }
}
