import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router, RouterEvent} from "@angular/router";
import {AuthenticationService} from "../service/authentication.service";
import {BehaviorSubject, Observable} from "rxjs";
import { UserService } from '../users/user.service';
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  isLoggedIn = false;

  //-------------
  LoginStatus$ : Observable<boolean>;
  UserName$ : Observable<string>;
  //-------------

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService,
              private userService: UserService) { }

  ngOnInit() {
    // this.isLoggedIn = this.authenticationService.isUserLoggedIn();
    // console.log('menu ->' + this.isLoggedIn);
    this.LoginStatus$ = this.authenticationService.isLoggedIn;
    this.UserName$ = this.authenticationService.currentUserName;

  }

  handleLogout() {
    this.authenticationService.logout();
  }

}
