import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserService} from './users/user.service';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { UserListComponent } from './users/user-list/user-list.component';
import { UpdateUserComponent } from './users/update-user/update-user.component';
import { CreateUserComponent } from './users/create-user/create-user.component';
import { UserDetailsComponent } from './users/user-details/user-details.component';
import { CreatePetComponent } from './pets/create-pet/create-pet.component';
import { UpdatePetComponent } from './pets/update-pet/update-pet.component';
import { PetDetailsComponent } from './pets/pet-details/pet-details.component';
import { PetListComponent } from './pets/pet-list/pet-list.component';
import {PetService} from "./pets/pet.service";
import {HttpInterceptorService} from "./service/http-interceptor.service";
import { MenuComponent } from './menu/menu.component';
import { AlertComponent } from './alert/alert.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserPetListComponent } from './user-pet-list/user-pet-list.component';
import {CommonModule, DatePipe} from "@angular/common";
import {BsDatepickerModule, BsDatepickerConfig} from 'ngx-bootstrap/datepicker';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { CreateAppointmentComponent } from './appointments/create-appointment/create-appointment.component';
import { DeleteAppointmentComponent } from './appointments/delete-appointment/delete-appointment.component';
import { AppointmentListComponent } from './appointments/appointment-list/appointment-list.component';
import { UpdateAppointmentComponent } from './appointments/update-appointment/update-appointment.component';
import { CreateRecordComponent } from './medical-records/create-record/create-record.component';
import { UpdateRecordComponent } from './medical-records/update-record/update-record.component';
import { RecordListComponent } from './medical-records/record-list/record-list.component';
import { DeleteRecordComponent } from './medical-records/delete-record/delete-record.component';
import {ProcedureComponent} from "./procedures/procedure.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    UserListComponent,
    UpdateUserComponent,
    CreateUserComponent,
    UserDetailsComponent,
    CreatePetComponent,
    UpdatePetComponent,
    PetDetailsComponent,
    PetListComponent,
    MenuComponent,
    AlertComponent,
    UserProfileComponent,
    UserPetListComponent,
    ProcedureComponent,
    CreateAppointmentComponent,
    DeleteAppointmentComponent,
    AppointmentListComponent,
    UpdateAppointmentComponent,
    CreateRecordComponent,
    UpdateRecordComponent,
    RecordListComponent,
    DeleteRecordComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [UserService, PetService, DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true,
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
